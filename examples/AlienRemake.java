import bcl.bcl;
import org.alterverse.control.*;
import org.alterverse.speech.tts;
import org.alterverse.sound.*;
import org.alterverse.game.*;
import org.alterverse.shapes.*;
import org.alterverse.world.*;

public class AlienRemake extends GameContext {
GameEngine engine;
int count;
Area area;
Sound theme;
double x;
// classes
class Alien extends MovableObject {
Sound move;
Sound land;
Sound explode;
public Alien(Shape shape) {
super(area,shape);
land=new Sound("sounds/land.ogg");
move=new Sound("sounds/alien.ogg");
explode = new Sound("sounds/explode.ogg");
// add sounds to list which should be moved with object
addSound(move);
addSound(explode);
addSound(land);
move.setPitch(Math.random()*0.4+0.9);
move.loop();
}

public void process() {
double nz = Math.random()*0.1;
double nx = Math.random()*1-0.5;
setPosition(getX()+nx,getY(),getZ()+nz);
if (getZ()>0) {
move.stop();
land.play();
area.remove(this);
removeSounds();
}
}

public void removeSounds() {
for (Sound s: getSounds())
SoundManager.removeSound(s);
}

public void onBump(GameObject othr) {
super.onBump(othr);

if (othr instanceof Beam) {
move.stop();
explode.play();
area.remove(this);
removeSounds();
}
}
}

class Beam extends MovableObject {
Sound laser;
public Beam(Shape shape) {
super(area,shape);
laser=new Sound("sounds/laser.ogg");
addSound(laser);
laser.play();
}

public void process() {
setPosition(getX(),getY(),getZ()-1);
if (getY()>20) {
area.remove(this);
removeSounds();
}
}

public void removeSounds() {
for (Sound s:getSounds())
SoundManager.removeSound(s);
}

public void onBump(GameObject othr) {
area.remove(this);
removeSounds();
}
}

public AlienRemake() {
engine = new GameEngine(this);
area = new Area();
area.setGravity(false);
tts.speak("Welcome to alien remake! ");
x=0;
count=0;
theme = new Sound("sounds/theme.mp3", true);
theme.setGain(0.5);
theme.loop();
engine.run();
}

public void process() {
count++;
// keys
if (bcl.isKeyDown(Keys.LEFT)) {
x-=0.125;
SoundManager.setListenerPosition(x,0,0);
}
if (bcl.isKeyDown(Keys.RIGHT)) {
x+=0.125;
SoundManager.setListenerPosition(x,0,0);
}
// other proccessing
if (count%150==0) {
// spaun new alien
double lx = Math.random()*10-5;
double lz = Math.random()*10-20;
area.add(new Alien(new Box(lx,-1,lz,2,2,2)));
}
area.process();
}

public void keyDown(int key) {
if (bcl.isKeyDown(Keys.LSHIFT))
switch(key) {
case 'q':
engine.quit();
break;
case 's':
area.saveJs("space.sav");
break;
case 'l':
area.loadJs("space.sav");
break;
}
else
switch(key) {
case 'i':
tts.speak(""+count);
break;
case ' ':
area.add(new Beam(new Box(x-0.125,-1,0,0.25,1,0.5)));
break;
}
}

public void destroy() {
theme.stop();
theme.destroy();}

public static void main(String [] args) {
new AlienRemake();
}
}
