import bcl.bcl;
import org.alterverse.world.*;
import org.alterverse.shapes.*;
import org.alterverse.sound.*;
import org.alterverse.game.*;
import org.alterverse.control.*;
import org.alterverse.speech.*;

public class WorldExample extends GameContext {
boolean editing = false;
GameEngine engine;
Area area;
class Floor extends GameObject {
public Floor(Shape shape) {
super(area,shape);
}
}

class Stair extends Floor {
public Stair(Shape shape) {
super(shape);
}
}

class Wall extends GameObject {
public Wall(Shape shape) {
super(area,shape);
}
}

class Machine extends GameObject {
Sound sound;
public Machine(Shape shape) {
super(area,shape);
sound = new Sound("sounds/machine1.ogg");
sound.loop();
addSound(sound);
updateData();
}
}

class Human extends DirectionalObject {
Sound land;
Sound wall;
Sound watch;
Sound stair;
public Human(Shape shape) {
super(area,shape);
land = new Sound("sounds/step.ogg");
wall = new Sound("sounds/wall.ogg");
addSound(wall);
addSound(land);
watch = new Sound("sounds/watch.ogg");
addSound(watch);
stair = new Sound("sounds/stair.ogg");
addSound(stair);
setStepupSize(0.5);
}

public void onFall(double distance) {
}

public void updateListener() {
super.updateData();
SoundManager.setListenerPosition(getX(),getY(),getZ());
}

public void onBump(GameObject othr) {
if (othr instanceof Wall)
wall.play();
else if (othr instanceof Stair)
stair.play();
else if (othr instanceof Floor)
land.play();
if (othr instanceof Human)
watch.play();
super.onBump(othr);
}
}

Player player;
double lx,ly,lz,ux,uy,uz;
public WorldExample() {
engine = new GameEngine(this);
lx=0;
ly=0;
lz=0;
ux=0;
uy=0;
uz=0;
area = new Area();
area.add(new Floor(new Box(-30,-2,-20,60,2,22)));
area.add(new Floor(new Box(-30,-7,-19,60,2,40)));
area.add(new Stair(new Box(-2,-1.5,2,4,1,1)));
area.add(new Stair(new Box(-2,-2,3,4,1,1)));
area.add(new Stair(new Box(-2,-2.5,4,4,1,1)));
area.add(new Stair(new Box(-2,-3,5,4,1,1)));
area.add(new Stair(new Box(-2,-3.5,6,4,1,1)));
area.add(new Stair(new Box(-2,-4,7,4,1,1)));
area.add(new Stair(new Box(-2,-4.5,8,4,1,1)));
area.add(new Stair(new Box(-2,-5,9,4,1,1)));
area.add(new Stair(new Box(-2,-5.5,10,4,1,1)));
area.add(new Wall(new Box(-30,0,-3,30,10,1)));
area.add(new Wall(new Box(-1,0,-6,1,10,4)));
area.add(new Wall(new Box(-1,0,-12,1,10,5)));
area.add(new Wall(new Box(-10,0,-12,1,10,10)));
area.add(new Wall(new Box(-9,0,-12,8,10,1)));
area.add(new Wall(new Box(1,0,-3,10,10,1)));
area.add(new Machine(new Box(-9,0,-11,2,2,1)));
Wall w = new Wall(new Box(1.0,0.0,-6.0,2.0,4.0,2.0));
w.addChild(new Wall(new Box(5.0,0.0,-6.0,2.0,4.0,2.0)));
w.addShape(new Box(9.0,0.0,-6.0,2.0,4.0,2.0));
area.add(w);
player =new Player(new Box(-0.2,2,0.2,0.4,1.9,0.4));
area.add(player);
area.add(new Nonplayer(new Box(-5,1,1,0.4,1.9,0.4)));
area.add(new Nonplayer(new Box(5,1,1,0.4,1.9,0.4)));
area.setGravity(true);
tts.speak("Starting up", 2);

engine.run();
}

public void process() {
area.process();
}

public class Nonplayer extends Human {
int timer=0;
public Nonplayer(Shape shape) {
super(shape);
}

public void process() {
timer++;
if (timer%20==0) {
double dx = (Math.random()*2)-1;
double dz = (Math.random()*2)-1;
setX(getX()+dx);
setZ(getZ()+dz);
}
}
}

public class Player extends Human {
public Player(Shape shape) {
super(shape);
setSpeed(0.5);
}

public void updateData() {
super.updateData();
SoundManager.setListenerPosition(getX(), getY(), getZ());
Point p = new Point(getDirection(),1);
SoundManager.setListenerOrientation(p.x,p.y,p.z,0,1,0);
}
}

public void keyDown(int key) {
if (editing) {
switch(key) {
case 's':
if (player.getSolid())
player.setSolid(false);
else
player.setSolid(true);
tts.speak(player.getSolid()? "Solid":"Non-solid", 2);
break;
case Keys.UP:
case 'i':
if (bcl.isKeyDown(Keys.LSHIFT))
player.setY(player.getY()+0.5);
player.updateListener();
break;
case Keys.DOWN:
case 'k':
if (bcl.isKeyDown(Keys.LSHIFT))
player.setY(player.getY()-0.5);
player.updateListener();
break;
}
}
switch(key) {
case 'e':
if (bcl.isKeyDown(Keys.LSHIFT) && bcl.isKeyDown(Keys.LALT)) {
if (editing)
editing = false;
else
editing = true;
tts.speak(editing? "Edit mode": "normal mode", 2);
area.setGravity(! editing);
}
break;
case Keys.LEFT:
case 'j':
if (bcl.isKeyDown(Keys.LSHIFT))
player.turnLeft();
else if (bcl.isKeyDown(Keys.LALT))
player.turnLeft90();
else
player.moveLeft();
break;
case Keys.RIGHT:
case 'l':
if (bcl.isKeyDown(Keys.LSHIFT))
player.turnRight();
else if (bcl.isKeyDown(Keys.LALT))
player.turnRight90();
else
player.moveRight();
break;
case Keys.UP:
case 'i':
if (! bcl.isKeyDown(Keys.LSHIFT))
player.moveForward();
break;
case Keys.DOWN:
case 'k':
if (! bcl.isKeyDown(Keys.LSHIFT))
player.moveBackward();
break;
case 'd':
tts.speak(""+player.getDirection(),2);
break;
case 'w':
tts.speak("position: " + player.getX()+", "+player.getY()+", " + player.getZ(),2);
break;
case 'a':
lx=player.getX();
ly = player.getY()-3.0;
lz = player.getZ();
break;
case 'b':
ux = player.getX();
uy=player.getY();
uz = player.getZ();
break;
case 'c':
GameObject gobj = new GameObject(area,new BoundBox(lx,ly,lz,ux,uy,uz));
GameObject candidate = area.getTouchingObjects(gobj).get(0);
ly = candidate.minY();
uy = candidate.maxY();
candidate.split(lx,ly,lz,ux,uy,uz);
break;
case 'q':
engine.quit();
break;
}
}

public static void main (String [] args) {
new WorldExample();
}
}
