import org.alterverse.speech.*;

public class Test {
public static void main(String [] args) {
EspeakSpeechEngine espeak = new EspeakSpeechEngine();
espeak.speak("Hello, world!");
 try {
System.in.read();
espeak.setRate(10);
espeak.speak("This is very slow.");
System.in.read();
espeak.setRate(95);
espeak.speak("This is very fast.");
System.in.read();
espeak.setRate(50);
espeak.setPitch(90);
espeak.speak("This is very high!");
System.in.read();
} catch(Exception x) {
}
espeak.destroy();
}
}
