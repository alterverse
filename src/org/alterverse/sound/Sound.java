/***************************************************************************
 *   Copyright (C) 2010 by the Alterverse team                             *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write see:                           *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package org.alterverse.sound;

import java.io.*;
import bcl.bcl;

public class Sound implements Serializable {
int sid;
boolean destroyed=false;
public Sound(String file) {
this(file,false);
}

public Sound(String file, boolean streaming) {
if (streaming)
sid = bcl.streamSound(file);
else
sid = bcl.loadSound(file);
}

public void setPosition(double x, double y, double z) {
bcl.setSoundPosition(sid,(float)x, (float)y, (float) z);
}

public void play() {
bcl.playSound(sid);
}

public void loop() {
bcl.loopSound(sid);
}
public void stop() {
bcl.stopSound(sid);
}

public void setPitch(double pitch) {
bcl.setSoundPitch(sid,(float) pitch);
}

public double getPitch() {
return 0;
}

public void destroy() {
destroyed=true;
bcl.unloadSound(sid);
}

public boolean isDestroyed() {
return destroyed;
}

public boolean isPlaying() {
return bcl.isSoundPlaying(sid);
}

public void setGain(double gain) {
bcl.setSoundGain(sid,(float) gain);
}
}
