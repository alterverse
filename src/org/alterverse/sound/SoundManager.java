/***************************************************************************
 *   Copyright (C) 2010 by the Alterverse team                             *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write see:                           *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package org.alterverse.sound;

import java.util.ArrayList;
import java.util.Iterator;
import bcl.bcl;

public class SoundManager {
static ArrayList<Sound> delsounds;

public static void init() {
bcl.initAudio();
delsounds=new ArrayList<Sound>();
}

public static void setListenerPosition(double x, double y, double z) {
bcl.setListenerPosition((float) x, (float) y, (float) z);
}

/*
public void setListenerOrientation(double fx, double fy, double fz, double ux, double uy, double uz) {
FloatBuffer ori = BufferUtils.createFloatBuffer(6).put(new float[] {(float) fx,(float) fy,(float) fz,(float) ux,(float) uy,(float) uz});
ori.flip();
AL10.alListener(AL10.AL_ORIENTATION,ori);
}
*/

public static void removeSound(Sound sound) {
if (sound.isDestroyed()) {
System.out.println("Already destroyed!");
return;
}
if (sound.isPlaying()) {
if (!delsounds.contains(sound))
delsounds.add(sound);
else
System.out.println("Already removed!");
}
else
sound.destroy();
}

public static void terminate() {
bcl.terminateAudio();
}

public static synchronized void process() {
Iterator<Sound> delit = delsounds.iterator();
while (delit.hasNext()) {
Sound i = delit.next();
if (! i.isPlaying()) {
i.destroy();
delit.remove();
}
}
}

public static void setListenerOrientation(double x1, double y1, double z1, double x2, double y2, double z2) {
bcl.setListenerOrientation((float)x1,(float)y1,(float)z1,(float)x2,(float)y2,(float)z2);
}
}
