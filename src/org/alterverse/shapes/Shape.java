/***************************************************************************
 *   Copyright (C) 2010 by the Alterverse team                             *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write see:                           *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package org.alterverse.shapes;

import java.io.Serializable;

public class Shape implements Serializable {
double x;
double y;
double z;
public Shape() {
}

public Shape(double x, double y, double z) {
this.x=x;
this.y=y;
this.z=z;
}

public void setPosition(double x, double y, double z) {
this.x=x;
this.y=y;
this.z=z;
}

public double minX() {
return x;
}

public double maxX() {
return x;
}

public double minY() {
return y;
}

public double maxY() {
return y;
}

public double minZ() {
return z;
}

public double maxZ() {
return z;
}

public boolean isTouching(Shape other) {
return !(minX()>other.maxX()||minY()>other.maxY()||minZ()>other.maxZ()||maxX()<other.minX()||maxY()<other.minY()||maxZ()<other.minZ());
}

public double getX() {
return (minX()+maxX())/2.0;
}

public double getY() {
return (minY()+maxY())/2.0;
}

public double getZ() {
return (minZ()+maxZ())/2.0;
}

public double getVolume() {
return (maxX()-minX())*(maxY()-minY())*(maxZ()-minZ());
}

public String toJs() {
return this.getClass().getName()+"("+x+","+y+","+z+")";
}
}
