/***************************************************************************
 *   Copyright (C) 2010 by the Alterverse team                             *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write see:                           *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package org.alterverse.shapes;

public class Box extends Shape {
double w;
double h;
double d;
public Box() {
w=0.0;
h=0.0;
d=0.0;
}

public Box(double x, double y, double z, double w, double d, double h) {
this.x=x;
this.y=y;
this.z=z;
this.w=w;
this.d=d;
this.h=h;
}

public void setPosition(double x, double y, double z) {
this.x=x-w/2;
this.y=y-d/2;
this.z=z-h/2;
}

public double maxX() {
return x+w;
}

public double maxY() {
return y+d;
}

public double maxZ() {
return z+h;
}

public String toJs() {
return this.getClass().getName()+"("+x+","+y+","+z+","+w+","+d+","+h+")";
}
}
