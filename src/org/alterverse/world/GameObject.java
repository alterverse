/***************************************************************************
 *   Copyright (C) 2010 by the Alterverse team                             *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write see:                           *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package org.alterverse.world;

import java.io.*;
import org.alterverse.shapes.*;
import org.alterverse.sound.*;
import java.util.ArrayList;
import java.util.Vector;

public class GameObject implements Serializable {
Vector<Shape> shapes;
double x;
double y;
double z;
double prevX;
double prevY;
double prevZ;
ArrayList<Sound> sounds;
boolean solid;
GameObject parent=null;
Area myArea;
Vector <GameObject> children;
public GameObject(Area area) {
this(area,0.0,0.0,0.0,new Shape());
}

public GameObject(Area area, Shape shape) {
this.x=(shape.minX()+shape.maxX())/2;
this.y=(shape.minY()+shape.maxY())/2;
this.z=(shape.minZ()+shape.maxZ())/2;
shapes = new Vector<Shape>();
addShape(shape);
solid=true;
sounds=new ArrayList<Sound>();
myArea=area;
prevX=x;
prevY=y;
prevZ=z;
children = new Vector<GameObject>();
}

public GameObject(Area area,double x,double y,double z,Shape shape) {
this.x=x;
this.y=y;
this.z=z;
shapes = new Vector<Shape>();
addShape(shape);
solid=true;
sounds=new ArrayList<Sound>();
myArea = area;
prevX=x;
prevY=y;
prevZ=z;
children = new Vector<GameObject>();
}

public boolean isTouching(GameObject other) {
for (Shape shape:shapes)
for (Shape shape2: other.getShapes())
if (shape.isTouching(shape2))
return true;
return false;
}

public void onBump(GameObject obj) {

}

public boolean canEnter(GameObject othr) {
return !(this.isSolid()&&othr.isSolid());
}

public boolean isSolid() {
return solid;
}

public void setSolid(boolean solid) {
this.solid=solid;
}

public boolean getSolid() {
return solid;
}

public void addSound(Sound sound) {
sounds.add(sound);
}

public void removeSound(Sound sound) {
sounds.remove(sound);
}

public ArrayList<Sound> getSounds() {
return sounds;
}

public boolean setPosition(double x, double y, double z) {
prevX=this.x;
prevY=this.y;
prevZ=this.z;
this.x = x;
this.y = y;
this.z = z;
return true;
}

public double getX() {
return x;
}

public double getY() {
return y;
}

public double getZ() {
return z;
}

public void updateData() {
for (Sound sound : sounds) {
sound.setPosition(x,y,z);
}
}

public void updateShape() {
for (Shape shape: shapes) {
double rx = shape.getX()-prevX;
double ry = shape.getY()-prevY;
double rz = shape.getZ()-prevZ;
shape.setPosition(this.x+rx,this.y+ry,this.z+rz);
}
}

public void setX(double x) {
setPosition(x,y,z);
}

public void setY(double y) {
setPosition(x,y,z);
}

public void setZ(double z) {
setPosition(x,y,z);
}

public Shape getShape() {
if (shapes.size()==0)
return null;
return shapes.get(0);
}

public void setShape(Shape shape) {
shapes.clear();
addShape(shape);
}

public Vector<Shape> getShapes() {
return shapes;
}

public void addShape(Shape shape) {
shapes.add(shape);
}

public Shape getShape(int i) {
if (i < shapes.size())
return shapes.get(i);
return null;
}

public void process() {
}


public double minX() {
if (shapes.size() == 0)
return 0;
double min = 0;
for (Shape shape: shapes)
if (shape.minX()<min)
min=shape.minX();
return min;
}

public double maxX() {
if (shapes.size() == 0)
return 0;
double max = 0;
for (Shape shape: shapes)
if (shape.maxX()>max)
max=shape.maxX();
return max;
}

public double minY() {
if (shapes.size() == 0)
return 0;
double min = 0;
for (Shape shape: shapes)
if (shape.minY()<min)
min=shape.minY();
return min;
}

public double maxY() {
if (shapes.size() == 0)
return 0;
double max = 0;
for (Shape shape: shapes)
if (shape.maxY()>max)
max=shape.maxY();
return max;
}

public double minZ() {
if (shapes.size() == 0)
return 0;
double min = 0;
for (Shape shape: shapes)
if (shape.minZ()<min)
min=shape.minZ();
return min;
}

public double maxZ() {
if (shapes.size() == 0)
return 0;
double max = 0;
for (Shape shape: shapes)
if (shape.maxZ()>max)
max=shape.maxZ();
return max;
}

public void addChild(GameObject child) {
children.add(child);
myArea.add(child);
child.setParent(this);
}

public void onRemove() {
for (GameObject obj: children)
myArea.remove(obj);
}

public void removeChild(GameObject child) {
children.remove(child);
myArea.remove(child);
child.setParent(null);
}

public GameObject getChild(int i) {
if (i < children.size())
return children.get(i);
return null;
}

public int numChildren() {
return children.size();
}

public boolean split(double plx, double ply, double plz, double pux, double puy, double puz) {
Vector <Shape> tmps = new Vector<Shape>();
Vector <Shape> remlist = new Vector<Shape>();
Vector<Shape> adlist = new Vector<Shape>();
int num=0;
BoundBox tester = new BoundBox(plx,ply,plz,pux,puy,puz);
for (Shape shape: shapes)
if (shape.isTouching(tester)) {
num++;
tmps.clear();
remlist.add(shape);
double lx = plx < shape.minX()? shape.minX() : plx;
double ly = ply < shape.minY()? shape.minY():ply;
double lz = plz < shape.minZ()?shape.minZ():plz;
double ux = pux > shape.maxX()?shape.maxX():pux;
double uy = puy>shape.maxY()?shape.maxY():puy;
double uz = puz > shape.maxZ()?shape.maxZ():puz;
tmps.add(new BoundBox(shape.minX(),shape.minY(),shape.minZ(),lx,shape.maxY(),shape.maxZ()));
tmps.add(new BoundBox(ux,shape.minY(),shape.minZ(),shape.maxX(),shape.maxY(),shape.maxZ()));
tmps.add(new BoundBox(lx,shape.minY(),shape.minZ(),ux,ly,shape.maxZ()));
tmps.add(new BoundBox(lx,uy,shape.minZ(),ux,shape.maxY(),shape.maxZ()));
tmps.add(new BoundBox(lx,ly,shape.minZ(),ux,uy,lz));
tmps.add(new BoundBox(lx,ly,uz,ux,uy,shape.maxZ()));
for (Shape s: tmps)
if (s.getVolume()>0.0) {
adlist.add(s);
org.alterverse.speech.tts.speak("added");
}
}
for (Shape s: adlist)
addShape(s);
for (Shape s: remlist)
shapes.remove(s);
if (num>0)
return true;
return false;
}

public void setParent(GameObject obj) {
parent=obj;
}

public GameObject getParent() {
return parent;
}

public String toJs(String varname) {
String ret = "var "+varname+" = "+this.getClass().getName()+"(area);\n";
ret+=varname+".setPosition("+x+","+y+","+z+");\n";
if (getSolid())
ret+=varname+".setSolid(true);\n";
else
ret+=varname+".setSolid(false);\n";
for (Shape s:shapes)
ret+=varname+".addShape("+s.toJs()+");\n";
return ret;
}
}
