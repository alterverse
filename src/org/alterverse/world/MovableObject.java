/***************************************************************************
 *   Copyright (C) 2010 by the Alterverse team                             *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write see:                           *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package org.alterverse.world;

import org.alterverse.shapes.*;

public class MovableObject extends GameObject {
double oldX;
double oldY;
double oldZ;
double fallDistance=0;
boolean landed = false;
public MovableObject(Area area,Shape shape) {
super(area,shape);
oldX=getX();
oldY=getY();
oldZ=getZ();
}
public MovableObject(Area area, double x, double y, double z, Shape shape) {
super(area, x,y,z,shape);
oldX=x;
oldY=y;
oldZ=z;
}

public void fall() {
if (oldX==this.x&&oldY<this.y&&oldZ==this.z)
fallDistance = 0;
else {
oldX=x;
oldY=y-10.0/32.0;
oldZ=z;
setY(getY()-10.0/32.0);
fallDistance+=(10.0/32.0);
onFall(fallDistance);
}
}

public void onFall(double distance) {
}


public boolean setPosition(double x, double y, double z) {
for (int i = 0; i < children.size(); i++) {
double rx = children.get(i).getX()-this.x;
double ry = children.get(i).getY()-this.y;
double rz = children.get(i).getZ()-this.z;
if (! children.get(i).setPosition(x+rx,y+ry,z+rz))
for (int j = 0; j < i; j++) {
double rx2 = children.get(j).getX()-x;
double ry2 = children.get(j).getY()-y;
double rz2 = children.get(j).getZ()-z;
children.get(j).setPosition(this.x+rx2,this.y+ry2,this.z+rz2);
}
}
prevX=this.x;
prevY=this.y;
prevZ=this.z;
this.x = x;
this.y = y;
this.z = z;
updateShape();
boolean moveBack=false;
for (GameObject obj:myArea.getTouchingObjects(this)) {
obj.onBump(this);
this.onBump(obj);
if (!obj.canEnter(this))
moveBack=true;
}
if (moveBack) {
this.x=prevX;
this.y=prevY;
this.z=prevZ;
prevX=x;
prevY=y;
prevZ=z;
updateShape();
return false;
}
else
updateData();
return true;
}

public void restoreMovement() {
oldX = x;
oldY = y;
oldZ = z;
}

public Point getNearestPoint(GameObject othr) {
double lx,ux,ly,uy,lz,uz;
if (othr.minX() > this.minX())
lx = othr.minX();
else
lx = this.minX();
if (othr.maxX() < this.maxX())
ux = othr.maxX();
else
ux = this.maxX();
if (othr.minY() > this.minY())
ly = othr.minY();
else
ly = this.minY();
if (othr.maxY() < this.maxY())
uy = othr.maxY();
else
uy = this.maxY();
if (othr.minZ() > this.minZ())
lz = othr.minZ();
else
lz = this.minZ();
if (othr.maxZ() < this.maxZ())
uz = othr.maxZ();
else
uz = this.maxZ();
if (ux<lx)
lx=ux;
if (uy<ly)
ly=uy;
if (uz<lz)
lz=uz;
Point ret = new Point();
ret.x = (lx+ux)/2;
ret.y = (ly+uy)/2;
ret.z = (lz+uz)/2;
return ret;
}

public void land(GameObject othr) {
if (! landed)
onLand(othr,fallDistance);
landed=true;
}

public void onLand(GameObject othr,double distance) {
}

public boolean hasLanded() {
return landed;
}

public void setLanded(boolean landed) {
this.landed = landed;
}
}
