package org.alterverse.story;

public class Story extends GameObject {
public Story(Building building, double x, double y, double z) {
super(building.getArea(),new Box(x,y,z,1.0,8.0,1.0));
}

public void resize() {
double mx=0;
double my = 0;
double mz = 0;
for (int i = 0; i < numChildren(); i++) {
GameObject child = getChild(i);
if (child.maxX()>mx)
mx=child.MaxX();
if (child.maxY()>my)
my = child.maxY();
if (child.maxZ()>mz)
mz = child.maxZ();
}
setShape(new Box(x,y,z,mx,my+8.0,mz));
}
}
