/***************************************************************************
 *   Copyright (C) 2010 by the Alterverse team                             *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write see:                           *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package org.alterverse.world;

import java.io.*;
import java.util.ArrayList;
import java.util.Vector;
import javax.script.*;

public class Area  implements Serializable {
ArrayList<GameObject> objects;
GameObject player = null;
Vector<GameObject> dellist;
boolean gravity;

public Area() {
objects = new ArrayList<GameObject>();
dellist=new Vector<GameObject>();
gravity = false;
}

public void process() {
for (int i = 0; i < objects.size();i++) {
// gravity stuff
if (gravity&& objects.get(i) instanceof MovableObject && !((MovableObject)objects.get(i)).hasLanded()) {
((MovableObject)objects.get(i)).fall();
}
// other stuff
objects.get(i).process();
}
for (GameObject obj : dellist) {
objects.remove(obj);
}
dellist.clear();
}

public void add(GameObject obj) {
objects.add(obj);
}

public void addAsPlayer(GameObject obj) {
add(obj);
setPlayer(obj);
}

public void remove(GameObject obj) {
dellist.add(obj);
}

public void save(String file) {
try {
ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
oos.writeObject(objects);
oos.close();
} catch(Exception x) {
x.printStackTrace();
}
}

public void load(String file) {
try {
ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
objects = (ArrayList<GameObject>) ois.readObject();
ois.close();
} catch(Exception x) {
x.printStackTrace();
}
}

public void setGravity(boolean gravity) {
this.gravity = gravity;
}

public boolean getGravity() {
return gravity;
}

public ArrayList<GameObject> getTouchingObjects(GameObject obj) {
ArrayList<GameObject> ret = new ArrayList<GameObject>();
for (GameObject i: objects) {
if (i==obj)
continue;
if (i.isTouching(obj))
ret.add(i);
}
return ret;
}

public void setPlayer(GameObject obj) {
player=obj;
}

public GameObject getPlayer() {
return player;
}

public boolean loadJs(String file, boolean fixedOnly) {
try {
ScriptEngineManager sm = new ScriptEngineManager();
ScriptEngine e = sm.getEngineByName("javascript");
if (fixedOnly) {
for (int i = 0; i < objects.size(); i++)
if (! (objects.get(i) instanceof MovableObject)) {
objects.remove(objects.get(i));
i--;
}
} else
objects.clear();
e.put("area",this);
e.eval(new FileReader(file));
return true;
} catch(Exception x) {
x.printStackTrace();
return false;
}
}

public void saveJs(String file,boolean fixedOnly) {
try {
FileWriter fw = new FileWriter(file);
if (getGravity())
fw.write("area.setGravity(true);\n");
else
fw.write("area.setGravity(false);\n");
for (GameObject obj: objects) {
if (fixedOnly&&obj instanceof MovableObject)
continue;
String var = "v"+System.currentTimeMillis();
fw.write(obj.toJs(var)+"\n");
fw.write("area.add("+var+");\n");
}
fw.close();
} catch(Exception x) {
x.printStackTrace();
}
}
}
