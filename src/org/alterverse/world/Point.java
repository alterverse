package org.alterverse.world;

public class Point {
public double x;
public double y;
public double z;
public Point() {
x=0;
y=0;
z=0;
}

public Point(double degrees, double speed) {
// ignore y
x=speed*Math.sin(Math.toRadians(degrees));
y = 0;
z = -speed*Math.cos(Math.toRadians(degrees));
}

public Point(double x, double y, double z) {
this.x = x;
this.y = y;
this.z = z;
}

public Point trim(double spead) {
return new Point(Math.min(x,spead),Math.min(y,spead), Math.min(z,spead));
}

public Point trim(Point p) {
return new Point(Math.min(x,p.x), Math.min(y,p.y), Math.min(z,p.z));
}

public Point bound(double min, double max) {
double lx = Math.abs(x);
double ly = Math.abs(y);
double lz = Math.abs(z);
double mx,my,mz;
if (lx<min)
mx=min;
else if (lx > max)
mx=max;
else
mx=lx;
if (ly<min)
my=min;
else if (ly > max)
my=max;
else
my=ly;
if (lz<min)
mz=min;
else if (lz > max)
mz=max;
else
mz=lz;
Point p=new Point(x,y,z);
if (p.x!=0)
p.x=p.x/lx*mx;
if (p.y!=0)
p.y=p.y/ly*my;
if (p.z!=0)
p.z=p.z/lz*mz;
return p;
}
}
