package org.alterverse.world;

public class Building extends GameObject {
public Building(Area area,double x, double y, double z) {
super(area,new Box(x,y,z,1.0,1.0,1.0));
setSolid(false);
}

public Story addStory() {
Story story;
if (numChildren() ==0)
story = new Story(this,x,y,z);
else
story = new Story(this,x,getChild(numChildren()-1).getY()+8.0,z);
addChild(story);
resize();
return story;
}

public void resize() {
double mx=0;
double my = 0;
double mz = 0;
for (int i = 0; i < numChildren(); i++) {
GameObject child = getChild(i);
if (child.maxX()>mx)
mx=child.MaxX();
if (child.maxY()>my)
my = child.maxY();
if (child.maxZ()>mz)
mz = child.maxZ();
}
setShape(new Box(x,y,z,mx,my+8.0,mz));
}
}
