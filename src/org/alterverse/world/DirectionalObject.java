package org.alterverse.world;

import org.alterverse.shapes.*;

public class DirectionalObject extends MovableObject {
double direction=0;
double speed=1;
double turnSize=5;
double stepupSize=0;
Point prev;
public DirectionalObject(Area area, Shape shape) {
super(area,shape);
prev=new Point();
}

public DirectionalObject(Area area, double x, double y, double z, Shape shape) {
super(area,x,y,z,shape);
prev=new Point();
}

public void setDirection(double degrees) {
direction=degrees;
}

public double getDirection() {
return direction;
}

public void setSpeed(double speed) {
this.speed = speed;
}

public void moveForward() {
stepup();
Point p = new Point(direction,speed);
setX(getX()+p.x);
setY(getY()+p.y);
setZ(getZ()+p.z);
}

public void moveLeft() {
stepup();
Point p = new Point(direction-90,speed);
setX(getX()+p.x);
setY(getY()+p.y);
setZ(getZ()+p.z);
}

public void moveRight() {
stepup();
Point p = new Point(direction+90,speed);
setX(getX()+p.x);
setY(getY()+p.y);
setZ(getZ()+p.z);
}

public void moveBackward() {
stepup();
Point p = new Point(direction+180,speed);
setX(getX()+p.x);
setY(getY()+p.y);
setZ(getZ()+p.z);
}

public void stepup() {
if (stepupSize==0)
return;
if (x==prev.x&&z==prev.z&&y==prev.y)
return;
setY(getY()+stepupSize);
prev.x=x;
prev.y=y;
prev.z=z;
}

public void turnRight() {
direction=(direction+360+turnSize)%360;
updateData();
}

public void turnLeft() {
direction=(direction+360-turnSize)%360;
updateData();
}

public double getTurnSize() {
return turnSize;
}

public void setTurnSize(double turnSize) {
this.turnSize = turnSize;
}

public void turnRight90() {
direction=(direction+360+90)%360;
updateData();
}

public void turnLeft90() {
direction=(direction+360-90)%360;
updateData();
}

public double getStepupSize() {
return stepupSize;
}

public void setStepupSize(double size) {
stepupSize=size;
}
}
