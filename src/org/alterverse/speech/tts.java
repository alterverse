package org.alterverse.speech;

import bcl.bcl;

public class tts {
public static final int SPEAK_INTERRUPTING=2;
public static final int SPEAK_BLOCKING = 1;
public static void init() {
bcl.initSpeech();
}

public static void terminate() {
bcl.terminateSpeech();
}

public static void speak(String text) {
speak(text,SPEAK_INTERRUPTING);
}

public static void speak(String text, int flags) {
bcl.speak(text,flags);
}
}
