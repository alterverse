/***************************************************************************
 *   Copyright (C) 2010 by the Alterverse team                             *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write see:                           *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package org.alterverse.control;

import bcl.bcl;
import java.io.*;

public class KeyboardHandler  implements Serializable {
KeyboardListener keyboardListener;
boolean listening;
public KeyboardHandler() {
this(null);
}

public KeyboardHandler(KeyboardListener keyboardListener) {
listening = false;
this.keyboardListener=keyboardListener;
}

public void listen() {
listening = true;
int key;
if (keyboardListener !=null) {
key = bcl.getPressedKey();
keyboardListener.keyDown(key);
key = bcl.getReleasedKey();
keyboardListener.keyUp(key);
}
}

public void destroy() {
listening=false;
}

public void setKeyboardListener(KeyboardListener keyboardListener) {
this.keyboardListener=keyboardListener;
}

public KeyboardListener getKeyboardListener() {
return keyboardListener;
}
}
