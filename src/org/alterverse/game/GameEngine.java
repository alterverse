/***************************************************************************
 *   Copyright (C) 2010 by the Alterverse team                             *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

package org.alterverse.game;

import bcl.bcl;
import org.alterverse.speech.tts;
import org.alterverse.control.*;
import org.alterverse.sound.SoundManager;
import java.io.*;
import java.util.Vector;

public class GameEngine  implements Serializable {
KeyboardHandler kh;
Vector<GameContext> gc;
boolean running;
int fps;

public GameEngine(GameContext gc) {
this(gc,32);
}
public GameEngine(GameContext context, int fps) {
gc = new Vector<GameContext>();
gc.add(context);
this.fps=fps;
running = false;
tts.init();
bcl.initEnvironment();
SoundManager.init();
kh = new KeyboardHandler(context);
// int [] lk = {Keyboard.KEY_LEFT, Keyboard.KEY_RIGHT,Keyboard.KEY_SPACE};
// kh=new KeyboardHandler(lk,gc);
}

public void run() {
// The main loop is here
running = true;
while (running) {
long cycleTime = System.currentTimeMillis();
// do our stuff
kh.listen();
gc.get(gc.size()-1).process();
SoundManager.process();
// pad our frame
int remaning = (int)cycleTime+1000/fps-(int)System.currentTimeMillis();
try {
Thread.sleep(Math.max(remaning,5));
} catch(Exception x) {
x.printStackTrace();
}
bcl.pumpEvents();
}
// our work is done
gc.get(0).destroy();
kh.destroy();
SoundManager.terminate();
bcl.terminateEnvironment();
tts.terminate();
}

public void addContext(GameContext context) {
gc.add(context);
kh = new KeyboardHandler(context);
context.create();
}

public GameContext getContext() {
return gc.get(gc.size()-1);
}

public void pop() {
if (gc.size()>1)
gc.remove(gc.get(gc.size()-1));
kh = new KeyboardHandler(gc.get(gc.size()-1));
}

public void quit() {
running = false;
}
}
