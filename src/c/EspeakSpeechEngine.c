/***************************************************************************
 *   Copyright (C) 2010 by the Alterverse team                             *
    *   email: rynkruger@gmail.com                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write see:                           *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

#include <jni.h>
#include <string.h>
#include <espeak/speak_lib.h>
#include "org_alterverse_speech_EspeakSpeechEngine.h"

/*
 * Class:     org_alterverse_speech_EspeakSpeechEngine
 * Method:    create
 * Signature: ()V
 */
JNIEXPORT jboolean JNICALL Java_org_alterverse_speech_EspeakSpeechEngine_create
  (JNIEnv *env, jobject jobj) {
if (espeak_Initialize(AUDIO_OUTPUT_PLAYBACK,100,NULL,1)<0)
return JNI_FALSE;
return JNI_TRUE;
}

/*
 * Class:     org_alterverse_speech_EspeakSpeechEngine
 * Method:    speak
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_org_alterverse_speech_EspeakSpeechEngine_speak
  (JNIEnv *env, jobject jobj, jstring jstr) {
const char * str = env->GetStringUTFChars(jstr,0);
int size = env->GetStringUTFLength(jstr);
espeak_Cancel();
espeak_Synth(str,size,0,POS_CHARACTER,0,0,NULL,NULL);
env->ReleaseStringUTFChars(jstr,str);
}

/*
 * Class:     org_alterverse_speech_EspeakSpeechEngine
 * Method:    getRate
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_alterverse_speech_EspeakSpeechEngine_getRate
  (JNIEnv *env, jobject jobj) {
float r = (espeak_GetParameter(espeakRATE,1)-espeakRATE_MINIMUM)/(espeakRATE_MAXIMUM/100);
return (jint) r;
}

/*
 * Class:     org_alterverse_speech_EspeakSpeechEngine
 * Method:    setRate
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_alterverse_speech_EspeakSpeechEngine_setRate
  (JNIEnv *env, jobject jobj, jint jrate) {
float r = (jrate*(espeakRATE_MAXIMUM/100))+espeakRATE_MINIMUM;
espeak_SetParameter(espeakRATE,(int) r,0);
}

/*
 * Class:     org_alterverse_speech_EspeakSpeechEngine
 * Method:    getPitch
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_alterverse_speech_EspeakSpeechEngine_getPitch
  (JNIEnv *env, jobject jobj) {
return espeak_GetParameter(espeakPITCH,1);
}

/*
 * Class:     org_alterverse_speech_EspeakSpeechEngine
 * Method:    setPitch
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_alterverse_speech_EspeakSpeechEngine_setPitch
  (JNIEnv *env, jobject jobj, jint jpitch) {
espeak_SetParameter(espeakPITCH,jpitch,0);
}

/*
 * Class:     org_alterverse_speech_EspeakSpeechEngine
 * Method:    getVoice
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_alterverse_speech_EspeakSpeechEngine_getVoice
  (JNIEnv *env, jobject jobj) {
}

/*
 * Class:     org_alterverse_speech_EspeakSpeechEngine
 * Method:    setVoice
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_alterverse_speech_EspeakSpeechEngine_setVoice
  (JNIEnv *env, jobject jobj, jint jvoice) {
}

/*
 * Class:     org_alterverse_speech_EspeakSpeechEngine
 * Method:    getAvailableVoices
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_org_alterverse_speech_EspeakSpeechEngine_getAvailableVoices
  (JNIEnv *env, jobject jobj) {
}

/*
 * Class:     org_alterverse_speech_EspeakSpeechEngine
 * Method:    destroy
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_org_alterverse_speech_EspeakSpeechEngine_destroy
  (JNIEnv *env, jobject jobj) {
espeak_Terminate();
}

